import time
import serial

class GrblStreamer:
    """
    Un objet python pour faire manger du gcode a hareng frit
    
    """
    def __init__(self, dev='/dev/ttyUSB0', baudrate=9600):
        self.dev = dev
        self.baudrate = baudrate
        self.feedrate = {'X' : 2000,'Y' : 2000, 'Z' : 180}
        self.serial = serial.Serial(dev, baudrate)
        self.serial.timeout = 0
        self.serial.write("\r\n\r\n")
        time.sleep(2)
        self.serial.flushInput()
        self.gcode_remove = ['G40', 'G49', 'G80', 'G54', 'G61']
		
    def setLocal(self):
        self.sendGcode('G21')
        self.sendGcode('G91')
		
    def sendGcode(self, gcode, wait = False):
        if gcode[-1]!= '\n':
            gcode += '\n'
        self.serial.write(gcode)
        message = self.waitOutput()
        if wait:
            while self.isMoving():
                time.sleep(0.01)
        return message
        
    def setAbcoluteCoords(self):
        return self.sendGcode('G90')
		
    def moveX(self, val, feedrate = None):
        return self.move('X', val, feedrate = feedrate)

    def moveY(self, val, feedrate = None):
        return self.move('Y', val, feedrate = feedrate)
		
    def moveZ(self, val, feedrate = None):
        return self.move('Z', val, feedrate = feedrate)
		
    def move(self, dir, val, feedrate = None):
        if not feedrate:
            feedrate = self.feedrate[dir]
        return self.sendGcode('G1 %s%s F%s\n'%(dir, val, feedrate))

    def setHome(self, axe = None):
        """
        axe = 'X' ou 'Y' ou 'Z'
        """
        if axe:
            message = self.sendGcode('G92 %s0'%axe)
        else:
            message = self.sendGcode('G92 X0 Y0 Z0')
        return message

    def waitOutput(self):
        """
        return a list of output
        """
        is_reading = True
        message = ''
        n_it = 0
        while is_reading:
            output = self.serial.readline()
            n_it += 1
            if output != '':
                message += output
                if output == 'ok\n\r':
                    is_reading = False
                n_it = 0
            elif output == '' and n_it < 2:
                time.sleep(0.05)
            else:
                is_reading = False
        message = message.split('\n')
        while '' in message:
            message.remove('')
        return message
        
    def isMoving(self):
        if self.getStatus() == 'Run':
            return True
        else :
            return False
        
    def getStatus(self):
        """
        get the status of grbl
        return 'Run', 'Idle' or 'Queue'
        """
        message = self.sendGcode('?\n')
        status = message[0]
        return status.split(',')[0][1:]
        
    def getPosition(self):
        """
        return a list of three coordinates
        """
        message = self.sendGcode('?\n')
        status = message[0]
        lst = status[status.find('MPos:')+5:status.find(',WPos')].split(',')
        for i in range(len(lst)):
            lst[i] = float(lst[i])
        return lst
        
    def setPause(self):
        if self.getStatus() == 'Queue':
            print 'set restart'
            return self.sendGcode('~\n')
        else:
            print 'set pause'
            return self.sendGcode('!\n')
            
    def getGcodeFromFile(self, file_name, replace_feedrate = True, clean = True):
        """
        pas encore teste et approuve
        """
        gcode = open(file_name, 'r')
        gcode = gcode.readlines()
        self.gcode_ogininal = gcode[:]
        if clean:
            gcode = self.cleanGcode(gcode)
            self.gcode_cleaned = gcode[:]
        if replace_feedrate:
            gcode = self.replaceFeedrate(gcode)
        self.gcode = gcode
            
    def replaceFeedrate(self, gcode):
        """
        pas encore teste et approuve
        """
        for i in range(len(gcode)):
            if 'X'in gcode[i] or 'Y' in gcode[i]:
                gcode[i] = 'G1 %s F%s \n'%(gcode[i][:gcode[i].find('\n')], self.feedrate['X'])
            elif 'Z' in gcode[i]:
                gcode[i] = '%s F%s \n'%(gcode[i][:gcode[i].find('\n')], 
                                        self.feedrate['Z'])
                if 'G0' in gcode[i]:
                    gcode[i] = gcode[i].replace('G0', 'G1')
                if gcode[i][:2] != 'G1':
                    gcode[i] = 'G1 %s' % gcode[i]
                if not 'G1' in gcode[i]:
                    print 'probleme ligne %s : %s'%(i, gcode[i])
                    
        return gcode

    def cleanGcode(self, gcode):
        cleaned_gcode = []
        for i in gcode:
            com = i
            if len(com)>0 and com[0]!=';' and com[:3] not in self.gcode_remove and com[0]!='S' and com[0]!='T':
                if ';' in com : com = com[:com.find(';')]
                if '(' in com : com = com[:com.find('(')]
                cleaned_gcode.append(com)
        return cleaned_gcode

    def sendGcodeSeq(self, by_step = True, verbose = True):
        for com in self.gcode:
            if verbose : print 'gcode : %s ' % com
            message = self.sendGcode(com, wait = True)
            if verbose : print 'output : %s ' % message
            if by_step:
                continuer = raw_input('continuer ? ')


